Questão 2

Área: Java

Categoria: Lógica de programação

Sub-categoria: Laços de repetição

Nível: Média

2) A única saída possível para o trecho de código abaixo é:

if(!true) {

    System.err.println("Condição 1 não é verdadeira");
} else if(true) {

    System.out.println("Condicao 2 é verdadeira");  
} else {

    System.err.println("Exit");  
}

a)Exit

b)Condição 1 não é verdadeira

c)Condição 2 é verdadeira

d)Condição 1 não é verdadeira / Exit