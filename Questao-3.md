Questão 3

Área: Java

Categoria: Lógica de programação

Sub-categoria: Função

Nível: Média

3) Qual a implementação da função que está sendo chamada a seguir: 


    System.out.println(somaDoisNumeros(5, 15));

a) public int somaDoisNumeros(int numero1, int numero2){ return numero1 + numero2;}

b) public void somaDoisNumeros(int numero1, int numero2){ return numero1 + numero2;}

c) public int somaDoisNumeros(){ return numero1 + numero2;}

d) public void somaDoisNumeros(int numero1, int numero2){ System.out.println(numero1 + numero2);}
