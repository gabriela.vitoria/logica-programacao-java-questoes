Questão 4

Área: Java

Categoria: Lógica de programação

Sub-categoria: Laços de repetição

Nível: Média

4) Considere o código a seguir e marque como ficaria um forEach:


    double[] cofre: {1.50, 9.0, 24.70};

a) forEach(double dinheiro: cofre) {System.out.println(dinheiro);}

b) for(double cofre: dinheiro) {System.out.println(cofre);}

c) for(double cofre: dinheiro) {System.out.println(dinheiro);}

d) for(double dinheiro: cofre) {System.out.println(dinheiro);}


