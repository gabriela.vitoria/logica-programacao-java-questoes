Questão 1

Área: Java

Categoria: Lógica de programação

Sub-categoria: Laços de repetição

Nível: Média

1) Analise o trecho de código a seguir e marque a opção que corresponde a saída dele:


   `int numero = 5;  
    do {
       System.out.println(numero);  
       numero++;  
    } while (numero <= 1);`

a) a saída é de 5 até 0

b) a saída é de 5 até 1

c) a saída é 5

d) Não existe do-while no Java.
